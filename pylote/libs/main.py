# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
# This is a part of Pylote project.
# Author:       Pascal Peter
# Copyright:    (C) 2008-2022 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


"""
DESCRIPTION :
    Contient l'interface graphique (fenêtre principale).
"""

# importation des modules utiles :
import sys
import json

import utils, utils_functions, utils_webengine, utils_filesdirs
import utils_config, utils_dialogs, utils_instruments, utils_graphicsview

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5 import QtPrintSupport
from PyQt5 import QtSvg


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, locale, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        """
        dd = QtWidgets.QApplication.desktop()
        print('screenCount : ', dd.screenCount())
        print('primaryScreen : ', dd.primaryScreen())
        print('isVirtualDesktop : ', dd.isVirtualDesktop())
        print('availableGeometry : ', dd.availableGeometry())
        for i in range(dd.screenCount()):
            print(i, 'availableGeometry : ', dd.availableGeometry(i))
        print('screenGeometry : ', dd.screenGeometry())
        for i in range(dd.screenCount()):
            print(i, 'screenGeometry : ', dd.screenGeometry(i))
        print('winId : ', dd.winId())
        """

        # Les chemins et autres trucs
        self.beginDir = QtCore.QDir.currentPath()
        self.filesDir = QtCore.QDir.homePath()
        self.appExt = QtWidgets.QApplication.translate(
            'main', 'Pylote Files (*.plt)')
        self.backgroundsDir = self.beginDir + '/images/backgrounds'
        self.pixmapDir = self.beginDir + '/images'
        self.tempPath = utils_filesdirs.createTempAppDir(
            utils.PROGNAME + '-temp')
        self.tempFileNum = -1
        self.configDir, first = utils_filesdirs.createConfigAppDir(
            utils.PROGNAME)
        utils.loadSupportedImageFormats()
        imagesFormats = ''
        for imageFormat in utils.SUPPORTED_IMAGE_FORMATS:
            imagesFormats = utils_functions.u(
                '{0}*.{1} ').format(
                    imagesFormats, 
                    utils_functions.u(imageFormat))
        utils.setSupportedImageFormatsText(imagesFormats)

        # Pour la fenêtre principale, l'i18n et on lit les Settings
        self.defaultFlags = self.windowFlags()
        self.setWindowFlags(
            self.defaultFlags | QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
        self.locale = locale

        # Pour l'affichage de l'image (GraphicsView) :
        self.view = utils_graphicsview.GraphicsView(self)
        self.setCentralWidget(self.view)
        self.view.setStyleSheet('background-color: transparent;')

        self.readSettings()
        utils_functions.myPrint('SCREEN_MODE:', utils.SCREEN_MODE)

        # Un premier Screenshot avant affichage de la fenêtre
        if utils.PYQT == 'PYQT5':
            screen = QtGui.QGuiApplication.primaryScreen()
            self.backgroundPixmap = screen.grabWindow(
                QtWidgets.QApplication.desktop().winId())
        else:
            self.backgroundPixmap = QtGui.QPixmap.grabWindow(
                QtWidgets.QApplication.desktop().winId())

        # Les instruments, dans une liste (y compris le faux curseur)
        self.ruler = utils_instruments.Ruler(self)
        self.rulerNotGraduated = utils_instruments.RulerNotGraduated(self)
        self.square = utils_instruments.Square(self)
        self.squareNotGraduated = utils_instruments.SquareNotGraduated(self)
        self.protractor = utils_instruments.Protractor(self)
        self.compass = utils_instruments.Compass(self)
        self.myCursor = utils_instruments.MyCursor(self)
        self.listeInstruments = (
            self.ruler, 
            self.rulerNotGraduated, 
            self.square, 
            self.squareNotGraduated,
            self.protractor, 
            self.compass, 
            self.myCursor)
        for instrument in self.listeInstruments:
            self.view.scene().addItem(instrument)
            instrument.setVisible(False)

        # Les actions et la TrayIcon
        self.createActions()
        if utils.WITH_TRAY_ICON:
            self.createTrayIcon()
            self.trayIcon.setIcon(utils.doIcon('icon.png'))
            self.trayIcon.show()
            self.trayIcon.activated.connect(self.iconActivated)

        # La boîte à outils :
        self.toolsWindow = utils_dialogs.ToolsWindow(self)
        self.toolsWindow.show()
        if self.configDict['TOLLBAR']['first']:
            self.configDict['TOLLBAR'][
                'windowWidth'] = self.toolsWindow.width() + 20
            size = QtCore.QSize(
                self.configDict['TOLLBAR']['windowWidth'], 
                self.configDict['TOLLBAR']['windowHeight'])
            self.toolsWindow.resize(size)
        self.firstRestore = [True, True]
        self.updateLastFilesMenu()

        # divers
        self.brush = QtGui.QBrush()
        self.drawingMode = 'NO'
        self.IsMinimized = False
        self.copiedItem = QtCore.QByteArray()

        self.fileName = ''
        # recherche d'un éventuel fichier plt passé en argument :
        for arg in sys.argv:
            extension = arg.split('.')[-1]
            if extension == 'plt':
                self.fileName = utils_functions.u(arg)
        self.mustSave = False
        self.updateTitle()

        QtCore.QTimer.singleShot(1, self.firstScreenShot)

    def interfaceLoaded(self):
        """
        fonction appelée une fois que l'interface est affichée.
        Permet de régler les positionnements des éléments
        """
        if self.configDict['TOLLBAR']['first']:
            self.configDict['TEMP']['TOLLBAR'][
                'windowGeometry'] = self.toolsWindow.saveGeometry()
            self.configDict['TEMP']['TOLLBAR'][
                'windowState'] = self.toolsWindow.saveState()
            self.configDict['TOLLBAR']['first'] = False
        else:
            self.toolsWindow.restoreGeometry(
                self.configDict['TEMP']['TOLLBAR']['windowGeometry'])
            self.toolsWindow.restoreState(
                self.configDict['TEMP']['TOLLBAR']['windowState'])
        self.toolsWindow.move(
            self.configDict['TOLLBAR']['windowX'], 
            self.configDict['TOLLBAR']['windowY'])
        self.toolsWindow.setWindowState(QtCore.Qt.WindowActive)
        self.view.setPenStyle(
            objectName=self.configDict['PEN']['style'])
        self.toolsWindow.draw()

    def resizeEvent(self, event):
        """
        gestion du décalage de l'image de fond à afficher
        et affichage d'icelle
        """
        self.position, self.decalage = self.calculDecalage()
        for i in range(10):
            QtCore.QCoreApplication.processEvents()
        brushTransform = QtGui.QTransform()
        brushTransform.translate(-self.decalage[0], -self.decalage[1])
        self.brush.setTransform(brushTransform)
        self.brush.setTexture(self.backgroundPixmap)
        self.view.scene().setBackgroundBrush(self.brush)
        # la suite a l'air nécessaire sous windows pour enlever les bordures :
        if sys.platform == 'win32':
            maskedRegion = QtGui.QRegion(
                0, 0, self.width(), self.height(), QtGui.QRegion.Rectangle)
            self.setMask(maskedRegion)

    def calculDecalage(self):
        """
        Calcul du décalage pour positionner la fenêtre
            et l'image à afficher
        Encore quelques bugs sur certains postes (windows !!!)
        """
        position = [0, 0]
        decalage = [0, 0]
        if utils.SCREEN_MODE == 'TESTS':
            rect = QtWidgets.QApplication.desktop().availableGeometry(
                self.screenNumber)
            position[0] = rect.x() + (rect.width() - self.width()) // 2
            position[1] = rect.y() + (rect.height() - self.height()) // 2
            self.move(position[0], position[1])
            decalage[0] = position[0] + self.width() // 2 - self.view.getContentsMargins()[0] // 2
            decalage[1] = position[1] + self.height() // 2 - self.view.getContentsMargins()[1] // 2
        elif utils.SCREEN_MODE == 'FULL_SPACE':
            # Position de la fenêtre en fonction de l'OS
            rect = QtWidgets.QApplication.desktop().availableGeometry(
                self.screenNumber)
            if sys.platform == 'win32':
                decalage[0] = rect.x()
                decalage[1] = rect.y()
                # cas windows géré à part :
                self. moveIfWin()
            else:
                position[0] = rect.x()# + (rect.width() - self.width()) // 2
                position[1] = rect.y()# + (rect.height() - self.height()) // 2                
                self.move(position[0], position[1])
            decalage[0] = position[0] + self.width() // 2 - self.view.getContentsMargins()[0] // 2
            decalage[1] = position[1] + self.height() // 2 - self.view.getContentsMargins()[1] // 2
        elif utils.SCREEN_MODE == 'FULL_SCREEN':
            decalage[0] = self.width() // 2
            decalage[1] = self.height() // 2    
        return position, decalage

    def moveIfWin(self):
        """
        En mode FULL_SPACE, je n'arrive pas à placer 
        correctement la fenêtre sous windows.
        Pour ce système, il vaut mieux utiliser le mode FULL_SCREEN.
        Cette procédure est donc à améliorer.
        Sous Linux, ça marche du tonnerre...
        """
        if utils.SCREEN_MODE == 'FULL_SPACE':
            origine = self.mapToGlobal(QtCore.QPoint(0, 0))
            #print(origine)
            rect = QtWidgets.QApplication.desktop().availableGeometry(
                self.screenNumber)
            """
            #print(self.view.getContentsMargins()[0])
            marge = (3 * self.view.getContentsMargins()[0],
                     3 * self.view.getContentsMargins()[1])
            #print(marge)
            self.move(rect.x() - origine.x() - marge[0],
                      rect.y() - origine.y() - marge[1])
            """
            # pourquoi 4 ?  Ça ne marche forcément pas tout le temps.
            a = 4
            self.move(
                rect.x() - origine.x() - a,
                rect.y() - origine.y() - a)

    def doMinimize(self):
        """
        Minimise toute l'interface.
        """
        self.IsMinimized = True
        self.doMinimizeToolsWindow()
        self.hide()

    def doMinimizeToolsWindow(self):
        """
        Minimise la ToolsWindow.
        Séparé de la procédure précédente car peut être appelé 
        indépendamment (double-clic par exemple).
        """
        self.toolsWindow.hide()

    def doRestore(self):
        """
        Restaure toute l'interface (en fonction de SCREEN_MODE).
        Gestion du cas windows
        """
        if utils.SCREEN_MODE == 'TESTS':
            self.show()
        elif utils.SCREEN_MODE == 'FULL_SPACE':
            rect = QtWidgets.QApplication.desktop().availableGeometry(
                self.screenNumber)
            self.resize(int(rect.width()), int(rect.height()))
            self.showMaximized()
        elif utils.SCREEN_MODE == 'FULL_SCREEN':
            self.showFullScreen()
        else:
            self.showFullScreen()
        self.doRestoreToolsWindow()
        self.IsMinimized = False
        if sys.platform == 'win32':
            self.moveIfWin()

    def changeEvent(self, event):
        #print('changeEvent', event.type())
        if event.type() == QtCore.QEvent.WindowStateChange:
            if not(self.isMinimized() or self.IsMinimized):
                self.doRestore()

    def doRestoreToolsWindow(self):
        """
        Restaure la ToolsWindow.
        """
        if self.toolsWindow.toolsKidMode:
            if self.firstRestore[1]:
                self.firstRestore[1] = False
        else:
            if self.firstRestore[0]:
                self.firstRestore[0] = False
                geometry = self.toolsWindow.geometry()
                frameGeometry = self.toolsWindow.frameGeometry()
                deltaX = frameGeometry.x() - geometry.x()
                deltaY = frameGeometry.y() - geometry.y()
                self.toolsWindow.move(
                    geometry.x() + deltaX, geometry.y() + deltaY)
        self.toolsWindow.show()
        self.toolsWindow.setWindowState(
            QtCore.Qt.WindowMinimized)
        self.toolsWindow.setWindowState(
            QtCore.Qt.WindowNoState | QtCore.Qt.WindowActive)

    def keyPressEvent(self, event):
        """
        La touche K bascule entre les modes "Kid" et normal.
        """
        if event.key() == QtCore.Qt.Key_K:
            self.switchKidMode()
        else:
            self.toolsWindow.keyPressEvent(event)

    def keyReleaseEvent(self, event):
        self.toolsWindow.keyReleaseEvent(event)

    def switchKidMode(self):
        """
        bascule entre les modes "Kid" et normal.
        """
        self.toolsWindow.toolsKidMode = not(self.toolsWindow.toolsKidMode)
        #self.toolsWindow.hide()

        if self.toolsWindow.toolsKidMode:
            modeFrom = 'TOLLBAR'
            modeTo = 'KID'
            flag = self.toolsWindow.kidFlags
        else:
            modeFrom = 'KID'
            modeTo = 'TOLLBAR'
            flag = self.toolsWindow.defaultFlags

        self.configDict['TEMP'][modeFrom][
            'windowGeometry'] = self.toolsWindow.saveGeometry()
        self.configDict['TEMP'][modeFrom][
            'windowState'] = self.toolsWindow.saveState()
        self.configDict[modeFrom]['windowX'] = self.toolsWindow.pos().x()
        self.configDict[modeFrom]['windowY'] = self.toolsWindow.pos().y()
        self.configDict[modeFrom][
            'windowWidth'] = self.toolsWindow.size().width()
        self.configDict[modeFrom][
            'windowHeight'] = self.toolsWindow.size().height()

        self.toolsWindow.setWindowFlags(flag)
        QtCore.QCoreApplication.processEvents()
        self.toolsWindow.reloadToolBar()
        self.doRestoreToolsWindow()

        self.toolsWindow.restoreGeometry(
            self.configDict['TEMP'][modeTo]['windowGeometry'])
        self.toolsWindow.restoreState(
            self.configDict['TEMP'][modeTo]['windowState'])
        self.toolsWindow.resize(
            self.configDict[modeTo]['windowWidth'], 
            self.configDict[modeTo]['windowHeight'])
        self.toolsWindow.move(
            self.configDict[modeTo]['windowX'], 
            self.configDict[modeTo]['windowY'])
        #self.toolsWindow.repaint()

    def newScreenshot(self):
        """
        On cache l'interface, puis on lance le ScreenShot.
        """
        self.doMinimize()
        QtCore.QTimer.singleShot(
            self.configDict['MAIN']['screenShotDelay'], 
            self.shootScreen)

    def shootScreen(self):
        """
        On fait le ScreenShot
        Puis on place l'image et on réaffiche
        """
        if utils.PYQT == 'PYQT5':
            screen = QtGui.QGuiApplication.primaryScreen()
            self.backgroundPixmap = screen.grabWindow(
                QtWidgets.QApplication.desktop().winId())
        else:
            self.backgroundPixmap = QtGui.QPixmap.grabWindow(
                QtWidgets.QApplication.desktop().winId())
        brushTransform = QtGui.QTransform()
        brushTransform.translate(-self.decalage[0], -self.decalage[1])
        self.brush.setTransform(brushTransform)
        self.brush.setTexture(self.backgroundPixmap)
        self.view.scene().setBackgroundBrush(self.brush)
        self.doRestore()
        self.saveTempFile()

    def firstScreenShot(self):
        """
        On place l'image du premier ScreenShot
        """
        brushTransform = QtGui.QTransform()
        brushTransform.translate(-self.decalage[0], -self.decalage[1])
        self.brush.setTransform(brushTransform)
        self.brush.setTexture(self.backgroundPixmap)
        self.view.scene().setBackgroundBrush(self.brush)
        if self.fileName != '':
            self.fileReload()
        self.saveTempFile(mustSave=False)

    def quit(self):
        """
        On vide le dossier temporaire et on écrit la config.
        On passe par qApp.quit() sinon Python3 ne quitte pas.
        """
        if self.testMustSave() == QtWidgets.QMessageBox.Cancel:
            return
        utils_filesdirs.emptyDir(self.tempPath)
        self.writeSettings()
        qApp = QtWidgets.QApplication.instance()
        qApp.quit()

    def readSettings(self):
        """
        récupération des réglages
        """
        if utils.SCREEN_MODE == 'TESTS':
            self.resize(QtCore.QSize(600, 400))

        self.configDict = {}
        configFileName = utils_functions.u(
            '{0}/config.json').format(self.configDir.canonicalPath())
        try:
            if QtCore.QFile(configFileName).exists():
                configFile = open(
                    configFileName, newline='', encoding='utf-8')
                self.configDict = json.load(configFile)
                configFile.close()
        except:
            self.configDict = {}
        # récupération ou initialisation des valeurs :
        for what in utils.DEFAULTCONFIG:
            if not(what in self.configDict):
                self.configDict[what] = utils.DEFAULTCONFIG[what]
            else:
                for key in utils.DEFAULTCONFIG[what]:
                    if not(key in self.configDict[what]):
                        self.configDict[what][
                            key] = utils.DEFAULTCONFIG[what][key]

        filesDir =  self.configDict.get('filesDir', '')
        if filesDir != '' and QtCore.QDir(filesDir).exists():
            self.filesDir = filesDir
        # on vérifie l'existence des fichiers :
        lastFiles = []
        for fileName in self.configDict['LASTFILES']:
            if QtCore.QFileInfo(fileName).isFile():
                lastFiles.append(fileName)
        self.configDict['LASTFILES'] = lastFiles

        # couleur et épaisseur du pinceau :
        actualColor = self.configDict['COLORS']['actualColor']
        color = self.configDict['COLORS'][actualColor]
        penColor = QtGui.QColor(color[0], color[1], color[2], color[3])
        self.view.drawPen.setColor(penColor)
        actualWidth = self.configDict['WIDTHS']['actualWidth']
        penWidth = self.configDict['WIDTHS'][actualWidth]
        self.view.drawPen.setWidth(penWidth)
        styles = {
            'Solid':  QtCore.Qt.SolidLine, 
            'Dash': QtCore.Qt.DashLine, 
            'Dot': QtCore.Qt.DotLine,
            'DashDot': QtCore.Qt.DashDotLine, 
            'DashDotDot': QtCore.Qt.DashDotDotLine, 
            }
        self.view.drawPen.setStyle(
            styles[self.configDict['PEN']['style']])

        # police :
        fontFamily = self.configDict['FONT']['family']
        fontPointSize = self.configDict['FONT']['pointSize']
        color = self.configDict['FONT']['color']
        fontColor = QtGui.QColor(color[0], color[1], color[2], color[3])
        font = QtGui.QFont(fontFamily)
        font.setPointSize(fontPointSize)
        self.view.font = font
        self.view.fontColor = fontColor

        # position et état de la barre d'outils :
        self.configDict['TEMP']['TOLLBAR'] = {
            'windowGeometry': None, 
            'windowState': None}
        self.configDict['TEMP']['KID'] = {
            'windowGeometry': None, 
            'windowState': None}
        if self.configDict['TOLLBAR']['first']:
            self.configDict['TOLLBAR'][
                'iconSize'] = utils.STYLE['PM_LargeIconSize']
            self.configDict['TOLLBAR'][
                'windowWidth'] =  15 * self.configDict['TOLLBAR']['iconSize']
            self.configDict['TOLLBAR'][
                'windowHeight'] = 2 * self.configDict['TOLLBAR']['iconSize']
            self.configDict['KID'][
                'iconSize'] = 2 * utils.STYLE['PM_LargeIconSize']
            self.configDict['KID'][
                'windowWidth'] =  4 * self.configDict['KID']['iconSize']
            self.configDict['KID'][
                'windowHeight'] = 1.5 * self.configDict['KID']['iconSize']
        for where in ('TOLLBAR', 'KID'):
            for what in ('windowGeometry', 'windowState'):
                value = self.configDict[where].get(what, None)
                value = QtCore.QByteArray().fromBase64(
                    QtCore.QByteArray().append(value))
                self.configDict['TEMP'][where][what] = value

        self.screenNumber = self.configDict.get('SCREEN_NUMBER', 0)
        utils.changeScreenNumber(self.screenNumber)
        if not(self.screenNumber < QtWidgets.QApplication.desktop().screenCount()):
            self.screenNumber = 0
        # on applique SCREEN_MODE s'il n'y a pas d'argument :
        screenMode = self.configDict.get('SCREEN_MODE', utils.SCREEN_MODE)
        if not(
            ('FULL_SCREEN' in sys.argv) \
            or ('FULL_SPACE' in sys.argv) \
            or ('TESTS' in sys.argv)):
                utils.changeScreenMode(screenMode)

    def writeSettings(self):
        """
        On sauvegarde les variables SCREEN_MODE, SCREEN_NUMBER etc...
        """
        self.configDict[
            'PROGVERSION'] = utils_functions.u(utils.PROGVERSION)
        if utils.SCREEN_MODE != 'TESTS':
            self.configDict['SCREEN_MODE'] = utils.SCREEN_MODE
        self.configDict['SCREEN_NUMBER'] = utils.SCREEN_NUMBER
        self.configDict['MAIN'][
            'filesDir'] = utils_functions.u(self.filesDir)
        self.configDict['MAIN'][
            'unitsLocked'] = self.toolsWindow.actionUnitsLock.isChecked()

        # visibilité des actions :
        self.configDict['TOLLBAR']['masked'] = []
        self.configDict['KID']['visible'] = []
        for (menu, actions) in self.toolsWindow.actions['LIST']:
            for action in self.toolsWindow.actions[actions]:
                if not(action in ('SEPARATOR', 'LASTFILES')):
                    actionName = action.objectName()
                    if not(self.toolsWindow.actionsState[action][1]):
                        self.configDict['TOLLBAR']['masked'].append(actionName)
                    if self.toolsWindow.actionsState[action][2]:
                        self.configDict['KID']['visible'].append(actionName)

        self.configDict['FONT']['family'] = self.view.font.family()
        self.configDict['FONT']['pointSize'] = self.view.font.pointSize()
        color = self.view.fontColor
        self.configDict['FONT']['color'] = (
                color.red(), 
                color.green(), 
                color.blue(), 
                color.alpha())

        self.configDict['TOLLBAR']['windowGeometry'] = utils_functions.u(
            self.toolsWindow.saveGeometry().toBase64())
        self.configDict['TOLLBAR']['windowState'] = utils_functions.u(
            self.toolsWindow.saveState().toBase64())
        self.configDict['TOLLBAR']['windowX'] = self.toolsWindow.pos().x()
        self.configDict['TOLLBAR']['windowY'] = self.toolsWindow.pos().y()
        if self.configDict['TOLLBAR']['windowX'] > 800:
            self.configDict['TOLLBAR']['windowX'] = 800
        if self.configDict['TOLLBAR']['windowY'] > 400:
            self.configDict['TOLLBAR']['windowY'] = 400
        self.toolsWindow.move(
            self.configDict['TOLLBAR']['windowX'], 
            self.configDict['TOLLBAR']['windowY'])
        self.configDict['TOLLBAR'][
            'windowWidth'] = self.toolsWindow.size().width()
        self.configDict['TOLLBAR'][
            'windowHeight'] = self.toolsWindow.size().height()

        self.configDict['KID']['windowGeometry'] = utils_functions.u(
            self.configDict['TEMP']['KID']['windowGeometry'].toBase64())
        self.configDict['KID']['windowState'] = utils_functions.u(
            self.configDict['TEMP']['KID']['windowState'].toBase64())

        # on ne garde que 10 fichiers :
        self.configDict['LASTFILES'] = self.configDict['LASTFILES'][:10]

        self.configDict['TEMP'] = {}
        # on écrit le fichier de config :
        configFileName = utils_functions.u(
            '{0}/config.json').format(self.configDir.canonicalPath())
        configFile = open(
            configFileName, 'w', encoding='utf-8')
        json.dump(self.configDict, configFile, indent=4)
        configFile.close()

    def configure(self):
        """
        Appel de la fenêtre de configuration du programme.
        On fait une sauvegarde avant pour restaurer
        correctement le fichier.
        """
        try:
            self.saveTempFile(mustSave=False)
            self.toolsWindow.hide()
            isTests = (utils.SCREEN_MODE == 'TESTS')
            dialog = utils_config.ConfigurationDlg(parent=self)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                self.toolsWindow.reloadToolBar()
                if utils.OS_NAME[0] != 'win':
                    self.setWindowFlags(self.defaultFlags)
                self.screenNumber = utils.SCREEN_NUMBER
                if not(self.screenNumber < QtWidgets.QApplication.desktop().screenCount()):
                    self.screenNumber = 0
                if isTests:
                    utils.changeScreenMode('TESTS')
                    size = QtCore.QSize(600, 400)
                else:
                    sg = QtWidgets.QApplication.desktop().screenGeometry(
                        self.screenNumber)
                    size = sg.size()
                if utils.OS_NAME[0] != 'win':
                    self.setWindowFlags(
                        self.defaultFlags | QtCore.Qt.FramelessWindowHint)
                for i in range(10):
                    QtCore.QCoreApplication.processEvents()
                """
                QtWidgets.QMessageBox.information(
                    self, 
                    '', 
                    'resize {0}x{1}'.format(size.width(), size.height()))
                """
                self.resize(size)
        finally:
            self.doRestore()
            self.toolsWindow.show()
            self.toolsWindow.repaint()
            for i in range(10):
                QtCore.QCoreApplication.processEvents()
            if not(isTests):
                #QtWidgets.QMessageBox.information(self, '', 'move')
                self.move(sg.topLeft())
            self.edit(what='UNDO')

    def createLinuxLauncher(self):
        """
        crée un lanceur (fichier .desktop).
        """
        try:
            self.toolsWindow.hide()
            title = QtWidgets.QApplication.translate(
                'main', 
                'Choose the Directory where the desktop file will be created')
            flag = (
                QtWidgets.QFileDialog.DontResolveSymlinks | 
                QtWidgets.QFileDialog.ShowDirsOnly)
            directory = QtWidgets.QFileDialog.getExistingDirectory(
                self,
                title,
                QtCore.QDir.homePath(),
                flag)
            if directory != '':
                import utils_filesdirs
                utils_functions.doWaitCursor()
                try:
                    result = utils_filesdirs.createLinuxDesktopFile(
                        self, 
                        self.beginDir, 
                        directory, 
                        utils.PROGLABEL)
                finally:
                    utils_functions.restoreCursor()
        finally:
            self.toolsWindow.show()

    def createActions(self):
        """
        Ces actions sont là car elles sont disponibles depuis la trayicon
        """
        self.actionQuit = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Quit'), 
            self, 
            icon=utils.doIcon('application-exit'),
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Quit))
        self.actionQuit.triggered.connect(self.quit)

        self.actionHelp = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Help'), 
            self, 
            icon=utils.doIcon('help-contents'),
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        self.actionHelp.triggered.connect(self.helpPage)

        self.actionAbout = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'About'), 
            self, 
            icon=utils.doIcon('help-about'))
        self.actionAbout.triggered.connect(self.about)

        self.actionNewScreenshot = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'New screenshot'), 
            self, 
            icon=utils.doIcon('camera-photo'))
        self.actionNewScreenshot.triggered.connect(self.newScreenshot)

        self.minimizeAction = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Minimize'), 
            self, 
            icon=utils.doIcon('go-down'))
        self.minimizeAction.triggered.connect(self.doMinimize)

        self.restoreAction = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Restore'), 
            self, 
            icon=utils.doIcon('view-restore'))
        self.restoreAction.triggered.connect(self.doRestore)

    def createTrayIcon(self):
        """
        Création de la trayicon et de son menu (obtenu par clic droit).
        """
        self.trayIconMenu = QtWidgets.QMenu(self)
        self.trayIconMenu.addAction(self.actionHelp)
        self.trayIconMenu.addAction(self.actionAbout)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.actionNewScreenshot)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.minimizeAction)
        self.trayIconMenu.addAction(self.restoreAction)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.actionQuit)
        self.trayIcon = QtWidgets.QSystemTrayIcon(self)
        self.trayIcon.setContextMenu(self.trayIconMenu)

    def iconActivated(self, reason):
        """
        Séparation entre clic et double-clic 
        (grâce à la variable iconDoubleClick).
        Un double-clic relance le screenshot.
        Le simple clic est géré après.
        """
        self.iconDoubleClick = False
        if reason == QtWidgets.QSystemTrayIcon.DoubleClick:
            self.iconDoubleClick = True
            self.newScreenshot()
        elif reason == QtWidgets.QSystemTrayIcon.Trigger:
            QtCore.QTimer.singleShot(500, self.iconClick)

    def iconClick(self):
        """
        Si on a bien à faire à un simple clic,
            on minimise ou restaure l'application.
        """
        if self.iconDoubleClick == False:
            if self.IsMinimized:
                self.doRestore()
            else:
                self.doMinimize()

    def unitsActions(self):
        """
        Gestion des actions (Save, Restore, Init) 
        liées aux unités des instruments.
        """
        what = self.sender().objectName()
        if what == 'UnitsSave':
            self.configDict['MAIN']['instrumentsScale'] = self.ruler.scale()
        elif what == 'UnitsRestore':
            for instrument in self.listeInstruments:
                if instrument.units:
                    instrument.setScale(
                        self.configDict['MAIN']['instrumentsScale'])
        elif what == 'UnitsInit':
            #self.configDict['MAIN']['instrumentsScale'] = 1
            for instrument in self.listeInstruments:
                if instrument.units:
                    instrument.setScale(1)

    def eraseAll(self, doSave=True):
        """
        Efface tous les items.
        """
        for item in self.view.scene().items():
            if not(item in self.listeInstruments):
                try:
                    data = int(item.data(QtCore.Qt.UserRole))
                    self.view.scene().removeItem(item)
                    del item
                except:
                    pass
        if doSave:
            self.saveTempFile()

    def deleteSelected(self):
        """
        Supprime l'item sélectionné, après demande de confirmation.
        """
        item = self.view.state['selected']
        if item != None:
            try:
                self.toolsWindow.hide()
                data = int(item.data(QtCore.Qt.UserRole))
                if QtWidgets.QMessageBox.question(
                    self, utils.PROGLABEL,
                    QtWidgets.QApplication.translate(
                        'main', 'Delete this item ?'),
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No
                    ) == QtWidgets.QMessageBox.Yes:
                    self.view.scene().removeItem(item)
                    del item
                self.view.state['selected'] = None
            except:
                pass
            finally:
                self.saveTempFile()
                self.toolsWindow.show()

    def editText(self):
        """
        Édition d'un texte ou du nom d'un point.
        """
        result = False
        item = self.view.state['selected']
        if item != None:
            if isinstance(item, QtWidgets.QGraphicsTextItem):
                result = True
                try:
                    self.toolsWindow.hide()
                    # pour distinguer entre un texte et un point :
                    isText = (item.data(QtCore.Qt.UserRole) != None)
                    initialText = item.toPlainText()
                    if isText:
                        dialog = utils_dialogs.TextItemDlg(
                            parent=self, 
                            graphicsTextItem=item)
                        if dialog.exec_() != QtWidgets.QDialog.Accepted:
                            item.setPlainText(initialText)
                        else:
                            self.saveTempFile()
                    else:
                        text, ok = QtWidgets.QInputDialog.getText(
                            self, 
                            utils.PROGLABEL,
                            QtWidgets.QApplication.translate(
                                'main', 'Point name:'), 
                            QtWidgets.QLineEdit.Normal,
                            initialText)
                        if ok:
                            item.setPlainText(text)
                            item.parentItem().text = text
                            self.saveTempFile()
                finally:
                    self.toolsWindow.show()
        return result

    def transparentArea(self):
        """
        Affiche une zone transparente
        """
        self.backgroundPixmap = QtGui.QPixmap()
        self.brush.setTexture(self.backgroundPixmap)
        self.view.scene().setBackgroundBrush(self.brush)
        self.view.setStyleSheet('background-color: transparent;')
        self.saveTempFile()

    def whitePage(self):
        """
        Affiche une page blanche
        """
        self.backgroundPixmap = QtGui.QPixmap()
        self.brush.setTexture(self.backgroundPixmap)
        self.view.scene().setBackgroundBrush(self.brush)
        self.view.setStyleSheet('background-color: white;')
        self.saveTempFile()

    def pointsPage(self):
        """
        Affiche une page de type papier pointé
        """
        image = QtGui.QImage('images/backgrounds/dots.svg')
        self.backgroundPixmap = QtGui.QPixmap.fromImage(image)
        self.brush.setTexture(self.backgroundPixmap)
        self.view.scene().setBackgroundBrush(self.brush)
        self.view.setStyleSheet('background-color: white;')
        self.saveTempFile()

    def gridPage(self):
        """
        Affiche une grille
        """
        image = QtGui.QImage('images/backgrounds/grid.svg')
        self.backgroundPixmap = QtGui.QPixmap.fromImage(image)
        self.brush.setTexture(self.backgroundPixmap)
        self.view.scene().setBackgroundBrush(self.brush)
        self.view.setStyleSheet('background-color: white;')
        self.saveTempFile()

    def chooseBackGround(self):
        """
        On sélectionne une image de fond
        """
        try:
            self.toolsWindow.hide()
            imagesFormats = utils_functions.u('{0} ({1})').format(
                QtWidgets.QApplication.translate('main', 'Image Files'), 
                utils.SUPPORTED_IMAGE_FORMATS_TEXT)
            fileName = QtWidgets.QFileDialog.getOpenFileName(
                self, 
                QtWidgets.QApplication.translate(
                    'main', 'Open Image'),
                self.backgroundsDir, 
                imagesFormats)
            if isinstance(fileName, tuple):
                fileName = fileName[0]
            if fileName == '':
                return
            self.backgroundsDir = QtCore.QFileInfo(fileName).absolutePath()
            image = QtGui.QImage(fileName)
            if image.width() > 400:
                self.backgroundPixmap = QtGui.QPixmap(
                    self.size().width(), 
                    self.size().height())
                self.backgroundPixmap.fill()
                if (image.width() > self.size().width()) \
                    or (image.height() > self.size().height()):
                        image = image.scaled(
                            self.size(), 
                            QtCore.Qt.KeepAspectRatio, 
                            QtCore.Qt.SmoothTransformation)
                painter = QtGui.QPainter(self.backgroundPixmap)
                painter.drawImage(
                    (self.size().width() - image.width()) // 2, 
                    (self.size().height() - image.height()) // 2, 
                    image)
                painter.end()
            else:
                self.backgroundPixmap = QtGui.QPixmap.fromImage(image)
            brushTransform = QtGui.QTransform()
            brushTransform.translate(-self.decalage[0], -self.decalage[1])
            self.brush.setTransform(brushTransform)
            self.brush.setTexture(self.backgroundPixmap)
            self.view.scene().setBackgroundBrush(self.brush)
            self.view.setStyleSheet('background-color: white;')
            self.saveTempFile()
        finally:
            self.toolsWindow.show()

    def fileSave(self):
        if self.fileName == '':
            self.fileSaveAs()
        else:
            try:
                self.toolsWindow.hide()
                self.saveFile(self.fileName)
            finally:
                self.toolsWindow.show()

    def selectFileName(self, title, proposedName, extension):
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, title, proposedName, extension)
        if isinstance(fileName, tuple):
            fileName = fileName[0]
        return fileName

    def fileSaveAs(self):
        """
        Enregistrement d'un fichier.
        On sauve le titre du programme en entête, 
            le fond et les items (sauf les instruments).
        """
        try:
            self.toolsWindow.hide()
            fileName = self.selectFileName(
                QtWidgets.QApplication.translate('main', 'Save File'), 
                self.filesDir, 
                self.appExt)
            if fileName == '':
                return
            utils_functions.doWaitCursor()
            fileName = utils_functions.verifieExtension(fileName, 'plt')
            self.saveFile(fileName)
            self.filesDir = QtCore.QFileInfo(fileName).absolutePath()
            self.fileName = fileName
            # on met à jour les fichiers récents :
            if (fileName in self.configDict['LASTFILES']):
                self.configDict['LASTFILES'].remove(fileName)
            self.configDict['LASTFILES'].insert(0, fileName)
            self.updateLastFilesMenu()
            self.updateTitle()
        finally:
            utils_functions.restoreCursor()
            self.toolsWindow.show()

    def saveFile(self, fileName):
        outFile = QtCore.QFile(fileName)
        if not outFile.open(QtCore.QFile.WriteOnly):
            QtWidgets.QMessageBox.warning(
                self, utils.PROGLABEL,
                QtWidgets.QApplication.translate(
                    'main', 'Failed to save\n  {0}').format(self.fileName))
            return
        try:
            stream = QtCore.QDataStream(outFile)
            stream.setVersion(QtCore.QDataStream.Qt_4_6)
            stream.writeQString(
                '{0}_{1}'.format(utils.PROGNAME, utils.PROGVERSION))
            stream << self.brush
            # on trie pour enregistrer dans l'ordre de création :
            draws = []
            for item in self.view.scene().items():
                if not(item in self.listeInstruments):
                    try:
                        data = int(item.data(QtCore.Qt.UserRole))
                        draws.append((item, data))
                    except:
                        pass
            draws = sorted(draws, key=lambda data: data[1])
            for (item, data) in draws:
                # pour chaque item, on enregistre son type (Point, ...) 
                # puis les attributs utiles
                if isinstance(item, utils_instruments.PointItem):
                    stream.writeQString('Point')
                    stream << item.pos() << item.sceneTransform() \
                        << item.pen() << item.brush() << item.font
                    stream.writeQString(item.text)
                    stream << item.textItem.pos() \
                        << item.textItem.itemTransform(item)[0]
                elif isinstance(item, QtWidgets.QGraphicsTextItem):
                    stream.writeQString('Text')
                    stream << item.pos() << item.sceneTransform() \
                            << item.font()  << item.defaultTextColor()
                    stream.writeQString(item.toPlainText())
                elif isinstance(item, QtWidgets.QGraphicsPixmapItem):
                    stream.writeQString('Pixmap')
                    stream << item.pos() << item.sceneTransform() \
                        << item.pixmap()
                elif isinstance(item, QtWidgets.QGraphicsPathItem):
                    stream.writeQString('Path')
                    stream << item.pos() << item.sceneTransform() \
                        << item.pen() << item.path()
                elif isinstance(item, QtWidgets.QGraphicsLineItem):
                    stream.writeQString('Line')
                    stream << item.pos() << item.sceneTransform() \
                            << item.pen() << item.line()
        except:
            QtWidgets.QMessageBox.warning(
                self, utils.PROGLABEL,
                QtWidgets.QApplication.translate(
                    'main', 
                    'Failed to save\n  {0}').format(self.fileName))
        finally:
            outFile.close()

    def saveTempFile(self, mustSave=True):
        self.tempFileNum += 1
        if self.tempFileNum > 9999:
            self.tempFileNum = 9999
        template = '{0}/{1}.plt'
        if self.tempFileNum < 10:
            template = '{0}/000{1}.plt'
        elif self.tempFileNum < 100:
            template = '{0}/00{1}.plt'
        elif self.tempFileNum < 1000:
            template = '{0}/0{1}.plt'
        fileName = utils_functions.u(
            template).format(self.tempPath, self.tempFileNum)
        self.saveFile(fileName)
        self.updateTitle(mustSave=mustSave)

    def updateTitle(self, mustSave=False):
        """
        mise à jour du titre de la fenêtre.
        Suivant que le fichier est enregistré, a été modifié, ...
        """
        self.mustSave = mustSave
        if mustSave:
            mustSaveText = '*'
        else:
            mustSaveText = ''
        if self.fileName == '':
            fileNameText = QtWidgets.QApplication.translate(
                'main', 'No name')
        else:
            fileNameText = QtCore.QFileInfo(self.fileName).baseName()
        title = utils_functions.u('{0} [{1}]{2}').format(
            utils.PROGLABEL, fileNameText, mustSaveText)
        self.toolsWindow.setWindowTitle(title)

    def testMustSave(self):
        """
        demande s'il faut enregistrer les modifications.
        """
        if self.mustSave:
            message = QtWidgets.QApplication.translate(
                'main', 'File must be saved ?')
            reply = QtWidgets.QMessageBox.question(
                self, 
                QtWidgets.QApplication.translate('main', 'Save'),
                message, 
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)
            if reply == QtWidgets.QMessageBox.Yes:
                if self.fileName == '':
                    self.fileSaveAs()
                elif self.saveFile(self.fileName):
                    self.mustSave = False
            return reply

    def edit(self, what=''):
        if what == 'COPY':
            item = self.view.state['selected']
            if item != None:
                data = int(item.data(QtCore.Qt.UserRole))
                self.view.state['selected'] = None

                clipboard = QtWidgets.QApplication.clipboard()
                mimeData = QtCore.QMimeData()
                mimeData.setData("application/x-pylote", QtCore.QByteArray())
                clipboard.setMimeData(mimeData)
                stream = QtCore.QDataStream(
                    self.copiedItem, QtCore.QIODevice.WriteOnly)
                if isinstance(item, utils_instruments.PointItem):
                    stream.writeQString('Point')
                    stream << item.pos() << item.sceneTransform() \
                        << item.pen() << item.brush() << item.font
                    stream.writeQString(item.text)
                    stream << item.textItem.pos() \
                        << item.textItem.itemTransform(item)[0]
                elif isinstance(item, QtWidgets.QGraphicsTextItem):
                    stream.writeQString('Text')
                    stream << item.pos() << item.sceneTransform() \
                            << item.font()  << item.defaultTextColor()
                    stream.writeQString(item.toPlainText())
                elif isinstance(item, QtWidgets.QGraphicsPixmapItem):
                    stream.writeQString('Pixmap')
                    stream << item.pos() << item.sceneTransform() \
                        << item.pixmap()
                elif isinstance(item, QtWidgets.QGraphicsPathItem):
                    stream.writeQString('Path')
                    stream << item.pos() << item.sceneTransform() \
                        << item.pen() << item.path()
                elif isinstance(item, QtWidgets.QGraphicsLineItem):
                    stream.writeQString('Line')
                    stream << item.pos() << item.sceneTransform() \
                            << item.pen() << item.line()
        elif what == 'PASTE':
            # récupération du presse-papier :
            newItem = None
            clipboard = QtWidgets.QApplication.clipboard()
            mimeData = clipboard.mimeData()
            if mimeData.hasFormat("application/x-pylote"):
                stream = QtCore.QDataStream(
                    self.copiedItem, QtCore.QIODevice.ReadOnly)
                newItem = self.readItemFromStream(stream)
            elif mimeData.hasImage():
                pixmap = clipboard.pixmap()
                newItem = QtWidgets.QGraphicsPixmapItem(pixmap)
                self.view.scene().addItem(newItem)
            elif mimeData.hasText():
                newItem = QtWidgets.QGraphicsTextItem()
                newItem.setFont(self.view.font)
                newItem.setDefaultTextColor(self.view.fontColor)
                newItem.setPlainText(clipboard.text())
                self.view.scene().addItem(newItem)
            if newItem != None:
                self.view.drawId += 1
                newItem.setData(QtCore.Qt.UserRole, self.view.drawId)
                newItem.setTransformOriginPoint(
                    newItem.boundingRect().center())
                if self.toolsWindow.actionDraw.isChecked():
                    if self.drawingMode == 'SELECT':
                        utils.doFlags(newItem, 'selectable')
                self.saveTempFile()
        elif what == 'UNDO':
            self.tempFileNum -= 1
            if self.tempFileNum < 0:
                self.tempFileNum = 0
            """
            QtWidgets.QMessageBox.information(
                self, '', 'UNDO {0}'.format(self.tempFileNum))
            """
            template = '{0}/{1}.plt'
            if self.tempFileNum < 10:
                template = '{0}/000{1}.plt'
            elif self.tempFileNum < 100:
                template = '{0}/00{1}.plt'
            elif self.tempFileNum < 1000:
                template = '{0}/0{1}.plt'
            fileName = utils_functions.u(
                template).format(self.tempPath, self.tempFileNum)
            self.openFile(fileName, doSave=False)
        elif what == 'REDO':
            self.tempFileNum += 1
            template = '{0}/{1}.plt'
            if self.tempFileNum < 10:
                template = '{0}/000{1}.plt'
            elif self.tempFileNum < 100:
                template = '{0}/00{1}.plt'
            elif self.tempFileNum < 1000:
                template = '{0}/0{1}.plt'
            fileName = utils_functions.u(
                template).format(self.tempPath, self.tempFileNum)
            if QtCore.QFileInfo(fileName).exists():
                self.openFile(fileName, doSave=False)
            else:
                self.tempFileNum -= 1
        elif what == 'DELETE':
            self.deleteSelected()
        elif what == 'ERASE':
            self.eraseAll()

    def fileNew(self):
        """
        """
        if self.testMustSave() == QtWidgets.QMessageBox.Cancel:
            return
        self.eraseAll()
        self.fileName = ''
        self.tempFileNum = -1
        self.newScreenshot()
        self.updateTitle()

    def fileOpen(self, fileName=''):
        """
        Ouverture d'un fichier.
        On sélectionne puis on délègue à la fonction fileReload.
        """
        if self.testMustSave() == QtWidgets.QMessageBox.Cancel:
            return
        try:
            self.toolsWindow.hide()
            if self.sender() != self.toolsWindow.actionFileOpen:
                # on a demandé un fichier récent :
                fileName = self.sender().data()
            else:
                fileName = QtWidgets.QFileDialog.getOpenFileName(
                    self,
                    QtWidgets.QApplication.translate(
                        'main', 'Open File'),
                    self.filesDir,
                    self.appExt)
                if isinstance(fileName, tuple):
                    fileName = fileName[0]
                if fileName == '':
                    return

            self.fileName = fileName
            if (fileName in self.configDict['LASTFILES']):
                self.configDict['LASTFILES'].remove(fileName)
            self.configDict['LASTFILES'].insert(0, fileName)
            self.updateLastFilesMenu()
            self.fileReload()
        finally:
            self.toolsWindow.show()

    def fileReload(self):
        """
        Réouverture du fichier. On commence par effacer tout.
        On vérifie l'entête, puis on récupère le fond et les items.
        """
        if self.fileName != '':
            self.filesDir = QtCore.QFileInfo(self.fileName).absolutePath()
            self.openFile(self.fileName)
            baseName = QtCore.QFileInfo(self.fileName).baseName()
            self.updateTitle()
        else:
            self.eraseAll()
        if self.toolsWindow.actionDraw.isChecked():
            if self.drawingMode == 'SELECT':
                for item in self.view.scene().items():
                    if not(item in self.listeInstruments):
                        utils.doFlags(item, 'selectable')
        self.saveTempFile(mustSave=False)

    def openFile(self, fileName, doSave=True):
        inFile = None
        try:
            inFile = QtCore.QFile(fileName)
            inFile.open(QtCore.QIODevice.ReadOnly)
            self.eraseAll(doSave=doSave)
            stream = QtCore.QDataStream(inFile)
            progNameVersion = stream.readQString()
            if not(utils.PROGNAME in progNameVersion):
                QtWidgets.QMessageBox.warning(
                    self, utils.PROGLABEL,
                    QtWidgets.QApplication.translate(
                        'main', 'not a valid file'))
                return
            if progNameVersion == utils.PROGNAME:
                progVersion = 0
            else:
                progVersion = int(10 * float(progNameVersion[-3:]))
            if progVersion < 14:
                stream.setVersion(QtCore.QDataStream.Qt_4_2)
            else:
                stream.setVersion(QtCore.QDataStream.Qt_4_6)
            self.readBackgroundFromStream(stream, progVersion)
            while not inFile.atEnd():
                item = self.readItemFromStream(stream, progVersion)
                if item != None:
                    item.setTransformOriginPoint(
                        item.boundingRect().center())
                    self.view.drawId += 1
                    item.setData(QtCore.Qt.UserRole, self.view.drawId)
        except:
            QtWidgets.QMessageBox.warning(
                self, utils.PROGLABEL, 
                QtWidgets.QApplication.translate(
                    'main', 'Failed to open\n  {0}').format(fileName))
        finally:
            if inFile != None:
                inFile.close()

    def fileGoPrevious(self):
        if self.fileName != '':
            listFiles = []
            dirIterator = QtCore.QDirIterator(self.filesDir, 
                                            ['*.plt'], 
                                            QtCore.QDir.Files)
            while dirIterator.hasNext():
                listFiles.append(dirIterator.next())
            listFiles = sorted(listFiles)
            i = listFiles.index(self.fileName)
            if i > 0:
                self.fileName = listFiles[i - 1]
                self.fileReload()

    def fileGoNext(self):
        if self.fileName != '':
            listFiles = []
            dirIterator = QtCore.QDirIterator(self.filesDir, 
                                            ['*.plt'], 
                                            QtCore.QDir.Files)
            while dirIterator.hasNext():
                listFiles.append(dirIterator.next())
            listFiles = sorted(listFiles)
            i = listFiles.index(self.fileName)
            if i < len(listFiles) - 1:
                self.fileName = listFiles[i + 1]
                self.fileReload()

    def exportToPainter(self, what, export='SVG'):
        """
        Crée un painter pour le paramètre what et dessine dessus.
        Retourne le painter.
        what peut être un générateur de SVG, un printer (imprimante ou PDF)
        """
        if export == 'SVG':
            scale = 1
            what.setSize(self.view.size())
            what.setResolution(int(1.25 * what.resolution()))
        elif export == 'PNG':
            scale = 1
        elif self.configDict['MAIN']['printMode'] == 'FullPage':
            scale = what.pageRect().width() / self.view.width()
            yscale = what.pageRect().height() / self.view.height()
            if yscale < scale:
                scale = yscale
        else:
            paperWidth = what.paperSize(
                QtPrintSupport.QPrinter.Millimeter).width()
            w = what.pageRect(QtPrintSupport.QPrinter.Millimeter).width()
            l = what.pageRect(QtPrintSupport.QPrinter.Millimeter).left()
            pageWidth = w - l
            scale = paperWidth / pageWidth
            #print('paperWidth :', paperWidth)
            #print('pageWidth :', pageWidth)
            #print('scale :', scale)

        def drawTextToPainter(painter, scale, textItem):
            """
            la taille et la position sont modifiées 
            (pourquoi et comment ?)
            Les 2 variables ont été trouvés par tests 
            (mais sont elles universelles ?)
            """
            if export == 'SVG':
                decalageFont = (5, 3)
                coeffFont = 1#1.3333
            elif export == 'PNG':
                decalageFont = (5, 3)
                coeffFont = 1#1.3333
            elif export in ('PDF', 'PRINT'):
                decalageFont = (5, 5)
                coeffFont = 2#1.3333
            painter.setPen(QtGui.QPen(textItem.defaultTextColor()))
            font = textItem.font()
            font.setPointSizeF(font.pointSize() * coeffFont / scale)
            painter.setFont(font)
            boundingRect = textItem.boundingRect()
            boundingRect.setWidth(boundingRect.width() * scale)
            boundingRect.setHeight(boundingRect.height() * scale)
            boundingRect.setX(decalageFont[0])
            boundingRect.setY(decalageFont[1])
            painter.drawText(boundingRect, textItem.toPlainText())

        if export == 'PNG':
            pixmap = QtGui.QPixmap(
                self.size().width(), 
                self.size().height())
            pixmap.fill()
            painter = QtGui.QPainter(pixmap)
            # pour avoir la transparence :
            painter.eraseRect(self.view.rect())
            painter.end()
            pixmap.setMask(pixmap.createHeuristicMask())
            # on ouvre à nouveau le QPainter pour tracer :
            painter.begin(pixmap)
        else:
            painter = QtGui.QPainter(what)
        painter.scale(scale, scale)
        # vérifier encore le décalage :
        originPoint = QtCore.QPoint(
            self.view.width() // 2 - self.decalage[0], 
            self.view.height() // 2 - self.decalage[1])
        brushWidth = self.brush.texture().width()
        if brushWidth == 0:
            brushWidth = 1
        brushHeight = self.brush.texture().height()
        if brushHeight == 0:
            brushHeight = 1
        if brushWidth < self.view.width():
            columns = 2 + self.view.width() // brushWidth
        else:
            columns = 1
        if brushHeight < self.view.height():
            rows = 2 + self.view.height() // brushHeight
        else:
            rows = 1
        if utils.SCREEN_MODE == 'TESTS':
            while originPoint.x() < - brushWidth:
                originPoint += QtCore.QPoint(brushWidth, 0)
            while originPoint.y() < - brushHeight:
                originPoint += QtCore.QPoint(0, brushHeight)
        painter.translate(originPoint)
        for i in range(columns):
            for j in range(rows):
                decalage = QtCore.QPoint(
                    i * brushWidth, 
                    j * brushHeight)
                painter.drawPixmap(decalage, self.brush.texture())

        draws = []
        for item in self.view.scene().items():
            if not(item in self.listeInstruments):
                try:
                    data = int(item.data(QtCore.Qt.UserRole))
                    draws.append((item, data))
                except:
                    pass
        draws = sorted(draws, key=lambda data: data[1])
        originPoint = QtCore.QPoint(
            self.size().width() // 2, 
            self.size().height() // 2)
        for (item, data) in draws:
            painter.resetTransform()
            painter.scale(scale, scale)
            painter.translate(originPoint)
            if isinstance(item, utils_instruments.PointItem):
                painter.setTransform(item.sceneTransform(), True)
                painter.setPen(item.pen())
                painter.setBrush(item.brush())
                painter.drawPath(item.path)
                painter.resetTransform()
                painter.scale(scale, scale)
                painter.translate(originPoint)
                painter.setTransform(item.textItem.sceneTransform(), True)
                drawTextToPainter(painter, scale, item.textItem)
            elif isinstance(item, QtWidgets.QGraphicsTextItem):
                painter.setTransform(item.sceneTransform(), True)
                drawTextToPainter(painter, scale, item)
            elif isinstance(item, QtWidgets.QGraphicsPixmapItem):
                painter.setTransform(item.sceneTransform(), True)
                #painter.drawPixmap(item.pos(), item.pixmap())
                painter.drawPixmap(0, 0, item.pixmap())
            elif isinstance(item, QtWidgets.QGraphicsPathItem):
                painter.setTransform(item.sceneTransform(), True)
                painter.setPen(item.pen())
                painter.setBrush(item.brush())
                painter.drawPath(item.path())
            elif isinstance(item, QtWidgets.QGraphicsLineItem):
                painter.setTransform(item.sceneTransform(), True)
                painter.setPen(item.pen())
                painter.drawLine(item.line())
        painter.end()
        if export == 'PNG':
            pixmap.save(what, 'PNG')

    def createPrinter(self, fileName=''):
        """
        Initialisation d'un printer.
        Si fileName != '', on fait un PDF
        """
        result = False
        utils_functions.doWaitCursor()
        try:
            printer = QtPrintSupport.QPrinter()
            printer.setPaperSize(QtPrintSupport.QPrinter.A4)
            if self.configDict['MAIN']['printOrientation'] == 'Portrait':
                printer.setOrientation(QtPrintSupport.QPrinter.Portrait)
            else:
                printer.setOrientation(QtPrintSupport.QPrinter.Landscape)
            printer.setColorMode(QtPrintSupport.QPrinter.Color)
            result = printer
            if fileName == '':
                # on lance le dialog d'impression :
                printer.setOutputFormat(QtPrintSupport.QPrinter.NativeFormat)
                utils_functions.restoreCursor()
                printDialog = QtPrintSupport.QPrintDialog(printer)
                if printDialog.exec_() != QtWidgets.QDialog.Accepted:
                    result = False
                utils_functions.doWaitCursor()
            else:
                # on veut un PDF :
                printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
                printer.setOutputFileName(fileName)
        finally:
            utils_functions.restoreCursor()
            return result

    def exportSVG(self):
        """
        Export du fichier en SVG, pour réutilisation dans Inkscape par exemple.
        """
        try:
            self.toolsWindow.hide()
            self.svgTitle = QtWidgets.QApplication.translate(
                'main', 'Export as SVG File')
            self.svgExt = QtWidgets.QApplication.translate(
                'main', 'SVG Files (*.svg)')
            if self.fileName != '':
                baseName = QtCore.QFileInfo(self.fileName).baseName()
                proposedName = utils_functions.u(
                    '{0}/{1}.svg').format(self.filesDir, baseName)
            else:
                proposedName = self.filesDir
            fileName = self.selectFileName(
                self.svgTitle, proposedName, self.svgExt)
            if fileName == '':
                return
            utils_functions.doWaitCursor()
            fileName = utils_functions.verifieExtension(fileName, 'svg')
            self.filesDir = QtCore.QFileInfo(fileName).absolutePath()
            generator = QtSvg.QSvgGenerator()
            generator.setFileName(fileName)
            self.exportToPainter(generator)
        finally:
            utils_functions.restoreCursor()
            self.toolsWindow.show()

    def exportPNG(self):
        """
        Export du fichier en PNG, pour réutilisation dans GIMP par exemple.
        """
        try:
            self.toolsWindow.hide()
            self.pngTitle = QtWidgets.QApplication.translate(
                'main', 'Export as PNG File')
            self.pngExt = QtWidgets.QApplication.translate(
                'main', 'PNG Files (*.png)')
            if self.fileName != '':
                baseName = QtCore.QFileInfo(self.fileName).baseName()
                proposedName = utils_functions.u(
                    '{0}/{1}.png').format(self.filesDir, baseName)
            else:
                proposedName = self.filesDir
            fileName = self.selectFileName(
                self.pngTitle, proposedName, self.pngExt)
            if fileName == '':
                return
            utils_functions.doWaitCursor()
            fileName = utils_functions.verifieExtension(fileName, 'png')
            self.filesDir = QtCore.QFileInfo(fileName).absolutePath()
            #generator = QtSvg.QSvgGenerator()
            #generator.setFileName(fileName)
            self.exportToPainter(fileName, export='PNG')
        finally:
            utils_functions.restoreCursor()
            self.toolsWindow.show()

    def exportPdf(self):
        try:
            self.toolsWindow.hide()
            self.pdfTitle = QtWidgets.QApplication.translate(
                'main', 'Export as PDF File')
            self.pdfExt = QtWidgets.QApplication.translate(
                'main', 'PDF Files (*.pdf)')
            if self.fileName != '':
                baseName = QtCore.QFileInfo(self.fileName).baseName()
                proposedName = utils_functions.u(
                    '{0}/{1}.pdf').format(self.filesDir, baseName)
            else:
                proposedName = self.filesDir
            fileName = self.selectFileName(
                self.pdfTitle, proposedName, self.pdfExt)
            if fileName == '':
                return
            utils_functions.doWaitCursor()
            fileName = utils_functions.verifieExtension(fileName, 'pdf')
            self.filesDir = QtCore.QFileInfo(fileName).absolutePath()
            printer = self.createPrinter(fileName)
            if printer == False:
                return
            self.exportToPainter(printer, export='PDF')
        finally:
            utils_functions.restoreCursor()
            self.toolsWindow.show()

    def documentPrint(self):
        try:
            self.toolsWindow.hide()
            printer = self.createPrinter()
            if printer == False:
                return
            self.exportToPainter(printer, export='PRINT')
        finally:
            self.toolsWindow.show()

    def updateLastFilesMenu(self):
        """
        mise à jour du menu des fichiers récents
        """
        self.toolsWindow.lastFilesMenu.clear()
        for fileName in self.configDict['LASTFILES']:
            newAction = QtWidgets.QAction(fileName, self)
            newAction.triggered.connect(self.fileOpen)
            newAction.setData(fileName)
            self.toolsWindow.lastFilesMenu.addAction(newAction)

    def readBackgroundFromStream(self, stream, progVersion):
        """
        On applique le fond du fichier
        """
        stream >> self.brush
        brushTransform = QtGui.QTransform()
        brushTransform.translate(-self.decalage[0], -self.decalage[1])
        self.brush.setTransform(brushTransform)
        self.view.scene().setBackgroundBrush(self.brush)

    def readItemFromStream(self, stream, progVersion=999):
        """
        Récupération des items et création
        """
        itemType = ''
        position = QtCore.QPointF()
        transform = QtGui.QTransform()
        itemType = stream.readQString()
        stream >> position >> transform
        if itemType == 'Point':
            pen = QtGui.QPen()
            brush = QtGui.QBrush()
            font = QtGui.QFont()
            text = ''
            textPos = QtCore.QPointF()
            stream >> pen >> brush >> font
            text = stream.readQString()
            stream >> textPos
            item = utils_instruments.PointItem(
                self, pen, brush, font, text)
            self.view.scene().addItem(item)
            if progVersion < 14:
                item.setPos(position)
                item.textItem.setPos(textPos)
            else:
                textTransform = QtGui.QTransform()
                stream >> textTransform
                item.setTransform(transform)
                item.textItem.setTransform(textTransform)
                textPos = QtCore.QPointF(0, 0)
                item.textItem.setPos(textPos)
        elif itemType == 'Text':
            font = QtGui.QFont()
            color = QtGui.QColor()
            text = ''
            stream >> font >> color
            text = stream.readQString()
            item = QtWidgets.QGraphicsTextItem()
            item.setPlainText(text)
            item.setFont(font)
            self.view.scene().addItem(item)
            item.setDefaultTextColor(color)
            if progVersion < 14:
                item.setPos(position)
            else:
                item.setTransform(transform)
        elif itemType == 'Pixmap':
            pixmap = QtGui.QPixmap()
            stream >> pixmap
            item = QtWidgets.QGraphicsPixmapItem(pixmap)
            self.view.scene().addItem(item)
            if progVersion < 14:
                item.setPos(position)
            else:
                item.setTransform(transform)
        elif itemType == 'Path':
            pen = QtGui.QPen()
            brush = QtGui.QBrush(QtCore.Qt.green, QtCore.Qt.NoBrush)
            path = QtGui.QPainterPath()
            stream >> pen >> path
            item = self.view.scene().addPath(path, pen, brush)
            if progVersion < 14:
                item.setPos(position)
            else:
                item.setTransform(transform)
        elif itemType == 'Line':
            pen = QtGui.QPen()
            line = QtCore.QLineF()
            stream >> pen >> line
            item = self.view.scene().addLine(line, pen)
            if progVersion < 14:
                item.setPos(position)
            else:
                item.setTransform(transform)
        else:
            item = None
        return item
        item.setTransformOriginPoint(item.boundingRect().center())
        self.view.drawId += 1
        item.setData(QtCore.Qt.UserRole, self.view.drawId)

    def about(self):
        """
        Affiche la fenêtre À propos
        """
        try:
            self.toolsWindow.hide()
            import utils_about
            aboutdialog = utils_about.AboutDlg(
                self, self.locale, icon='./images/icon.png')
            aboutdialog.resize(700, 500)
            aboutdialog.exec_()
        finally:
            self.toolsWindow.show()

    def helpPage(self):
        """
        Affiche la page d'aide dans le navigateur.
        """
        import utils_about
        theUrl = utils.HELPPAGE
        utils_about.webHelpInBrowser(theUrl)


