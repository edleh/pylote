# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
# This is a part of Pylote project.
# Author:       Pascal Peter
# Copyright:    (C) 2008-2022 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient des fonctions utiles au programme.
"""


# importation des modules utiles :
import sys
import os

# importation des modules perso :
import utils

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui



###########################################################"
#   POUR L'AFFICHAGE DES TEXTES
###########################################################"

def myPrint(*args):
    if len(args) > 1:
        print(args)
    else:
        arg = args[0]
        try:
            print(arg)
        except:
            try:
                print(u(arg))
            except:
                try:
                    print(s(arg))
                except:
                    print('PB in myPrint')

def u(text):
    """
    retourne une version unicode de text
    returns a unicode version of text
    """
    result = text
    try:
        if isinstance(text, QtCore.QByteArray):
            result = bytes(text).decode('utf-8')
        elif not(isinstance(text, str)):
            result = str(text)
    except:
        try:
            if not(isinstance(text, str)):
                result = str(text)
        except:
            myPrint('ERROR utils.u', type(text), text)
    # résolution d'un problème avec les QByteArray 
    # (b'aaa' au lieu de 'aaa') :
    if result[:2] in ("b'", 'b"'):
        result = result[2:-1]
    return result

def s(text, encoding=''):
    # retourne une version str de text
    if isinstance(text, str):
        if encoding == '':
            return text
        else:
            return text.encode(encoding)
    else:
        try:
            return str(text)
        except:
            myPrint('ERROR utils.s', type(text), text)
            return text



###########################################################"
#   MESSAGES, BOUTONS, ...
###########################################################"

NOWAITCURSOR = False
def changeNoWaitCursor(newNoWaitCursor):
    global NOWAITCURSOR
    NOWAITCURSOR = newNoWaitCursor

def doWaitCursor():
    if not(NOWAITCURSOR):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

def restoreCursor():
    if not(NOWAITCURSOR):
        QtWidgets.QApplication.restoreOverrideCursor()



###########################################################"
#   DIVERS
###########################################################"

def doLocale(locale, beginFileName, endFileName, defaultFileName=''):
    """
    Teste l'existence d'un fichier localisé.
    Par exemple, insère _fr_FR ou _fr entre beginFileName et endFileName.
    Renvoie le fichier par défaut sinon.
    """
    # on teste d'abord avec locale (par exemple fr_FR) :
    localeFileName = u('{0}_{1}{2}').format(
        beginFileName, locale, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # ensuite avec lang (par exemple fr) :
    lang = locale.split('_')[0]
    localeFileName = u('{0}_{1}{2}').format(
        beginFileName, lang, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # si defaultFileName est spécifié :
    if defaultFileName != '':
        return u(defaultFileName)
    # sinon on renvoie le fichier de départ :
    localeFileName = u('{0}{1}').format(
        beginFileName, endFileName)
    return localeFileName

def addSlash(aDir):
    """
    pour ajouter un / à la fin d'un nom de dossier si besoin
    aDir = utils_functions.addSlash(aDir)
    """
    if aDir[-1] != '/':
        aDir = aDir + '/'
    return aDir

def verifieExtension(fileName, extension):
    """
    Pour ajouter l'extension au nom du fichier si besoin
    """
    r = fileName.split('.')[0] + '.' + extension
    return r


